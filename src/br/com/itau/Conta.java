package br.com.itau;

public class Conta {
    private Cliente cliente;
    private String agencia;
    private String conta;
    private double saldo;
    private double valor;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Conta(Cliente cliente, String agencia, String conta, double saldo, double valor) {
        this.cliente = cliente;
        this.agencia = agencia;
        this.conta = conta;
        this.saldo = saldo;
        this.valor = valor;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double sacar (){
        this.saldo = this.saldo - this.valor;
        return this.saldo;
    }

    public double depositar (){
        this.saldo = this.saldo + this.valor;
        return this.saldo;
    }

}
