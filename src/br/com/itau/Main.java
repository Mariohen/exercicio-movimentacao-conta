package br.com.itau;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import org.w3c.dom.ls.LSOutput;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
	  try {
		  Cliente cliente1 = new Cliente("Estanilau da silva", "111.222.333-44", 17);
		  System.out.println("Cliente " + " permitido para abrir conta");

		  Conta conta1 = new Conta(cliente1, "1111",
				  "88888", 1000.00, 100.00);

		  conta1.depositar();
		  System.out.println(conta1.getSaldo());

		  conta1.sacar();
		  conta1.sacar();
		  System.out.println(conta1.getSaldo());
	  }
	  catch (ArithmeticException e) {
		  System.out.println("Teste - " + e);
	  }

		Random ran = new Random();
		System.out.println(ran.nextInt(10));


    }

}
